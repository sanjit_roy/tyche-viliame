import debug from 'debug';
import express from 'express';
import http from 'http';
import cors from 'cors';

const app: express.Application = express();

const server: http.Server = http.createServer(app);
const port = 3000;

const debugLog: debug.IDebugger = debug('app');

app.use(express.json());
app.use(cors);

app.get('/', (req: express.Request, res: express.Response) => {
  res.status(200).send(`Server running at PORT ${port}`);
});

server.listen(port, () => {
  console.log(`Server running at PORT ${port}`);
});
